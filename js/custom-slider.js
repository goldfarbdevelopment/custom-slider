export default class CustomSlider {
    constructor (slides) {
        this.slides = slides;
        this.containerWidth = $('.custom-slider-slides').width();
        this.trackWidth = this.containerWidth*(slides.length);
        this.track = $('.custom-slider-track');
        this.prevNext = $('.custom-slider-button-left, .custom-slider-button-right');
        this.navigation = $('.custom-slider-navigation');
        this.active = false;
        this.currentX;
        this.initialX;
        this.xOffset = 0;
    }

    /**
     * Activates the current slide item and sets up the prev and next items
     * @param item
     */
    activateItem (item) {
        $('.active, .nextItem, .prevItem').removeClass('active nextItem prevItem');
        item.addClass('active');

        if(item.next().length === 0) {
            $('.custom-slider-slides .slide[data-index="0"]').addClass('nextItem');
        }
        const prevItem = item.prev().length ? item.prev() : $(`.slide[data-index="${this.slides.length-1}"]`);
        const nextItem = item.next().length ? item.next() : $('.slide[data-index="0"]');
        prevItem.addClass('prevItem');
        nextItem.addClass('nextItem');

        return this.updateNavigation(item.data('index'));
    }

    /**
     * Initiates the build process for the slider
     */
    build() {

        //Loop through the array of slides and append to track and nav
        this.slides.forEach((slide, index) => {
            const itemContent = this.buildSlideContent(slide, index);
            const navItem = this.buildNavItem(slide, index);

            $('.custom-slider-navigation').append(navItem);
            this.track.append(itemContent);
        });

        let activeItem = $('.active');

        this.track.width(this.trackWidth);
        $('.slide').width(this.containerWidth);

        // If array of slides not setup with at least one active slide,
        // activate first slide and nav item.
        if (activeItem.length === 0) {
            activeItem = $('.slide[data-index="0"]');
            activeItem.addClass('active');
            $('.form-check-input[data-index="0"]').prop('checked', 'checked');
        }

        this.activateItem(activeItem);

        //todo add case for mobile vs. desktop
        this.activateDrag(this.track);
        this.navigationSetup();
    }

    /**
     * Builds html for nav item
     * @param {object} slide
     * @param {number} index
     * @returns {string}
     */
    buildNavItem (slide, index) {
        const active = slide.active ? 'checked="checked"' : '';
        const navItem = `<div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" data-index="${index}" ${active} value="${index}"/>
                         </div>`;
        return navItem;
    }

    /**
     * Builds HTML for slide item
     * @param {object} slide
     * @param {number} index
     * @returns {string}
     */
    buildSlideContent (slide, index) {
        const active = slide.active ? ' active' : '';
        let slideContent = `<div class="slide${active}" data-index="${index}">`;

        if (slide.content) {
            slideContent += slide.content;
        } else {
            slideContent += `<img src="${slide.url}" alt="${slide.name}"/>`;
        }

        slideContent += '</div>';
        return slideContent;
    }

    /**
     * Add listeners for nav buttons
     */
    navigationSetup () {
        this.prevNext.click(e =>{
            if($(e.currentTarget).hasClass('custom-slider-button-left')) {
                this.prev();
            } else {
                this.next();
            }
        });

        $('.custom-slider-navigation input').change((e) => {
            const navItem = $(e.target);
            const navIndex = navItem.data('index');
            const active = $('.active');

            if (active.data('index') !== navIndex) {
               active.removeClass('active');
               this.activateItem($(`.slide[data-index="${navIndex}"]`));
            }

            this.updateNavigation(navIndex);
        });
    }

    //todo prev and next should be combined into 1 function.

    /**
     * Next slide
     */
    next () {
        const active = $('.active');
        const next = active.next().length > 0 ? active.next() : $(`.slide[data-index="0"]`);
        this.activateItem(next);
    }

    /**
     * Prev slide
     */
    prev () {
        const active = $('.active');
        const prev = active.prev().length > 0 ? active.prev() : $(`.slide[data-index="${this.slides.length-1}"]`);
        this.activateItem(prev);
    }

    /**
     * Update navigation when slide changes
     * @param {number} index
     */
    updateNavigation (index) {
        this.navigation.find('input').prop('checked', '');
        this.navigation.find(`input[data-index="${index}"`).prop('checked', 'checked');
    }


    // DRAG UX
    /**
     * Initiate drag listeners
     */
    activateDrag() {
        this.track.on("touchstart", this, this.dragStart);
        this.track.on("touchend", this, this.dragEnd);
        this.track.on("touchmove", this, this.drag);

        this.track.on("mousedown", this, this.dragStart);
        this.track.on("mouseup", this, this.dragEnd);
        this.track.on("mousemove", this, this.drag);
    }

    /**
     * Initiates drag start
     * @param {object} e
     */
    dragStart(e) {
        e.data.track.addClass('drag');

        e.data.startX = e.clientX;
        if (e.type === "touchstart") {
            e.data.initialX = e.touches[0].clientX -  e.data.xOffset;
        } else {
            e.data.initialX = e.clientX -  e.data.xOffset;
        }

        if ($(e.target).parents().hasClass('active')) {
            e.data.active = true;
        }
    }

    /**
     * Initiates drag stop
     * @param e
     */
    dragEnd(e) {
        if (e.data.currentX < 0 && e.data.currentX <= e.data.containerWidth/2) {
            e.data.next();
        }

        if (e.data.currentX > 0 && e.data.currentX >= e.data.containerWidth/2) {
            e.data.prev();
        }

        e.data.xOffset = 0;
        e.data.setTranslate( 0,  e.data.track);

        e.data.active = false;

        e.data.track.removeClass('drag');
    }

    /**
     * Updates drag item location
     * @param e
     */
    drag(e) {
        if ( e.data.active) {

            e.preventDefault();

            if (e.type === "touchmove") {
                e.data.currentX = e.touches[0].clientX -  e.data.initialX;
            } else {
                e.data.currentX = e.clientX -  e.data.initialX;
            }

            e.data.xOffset =  e.data.currentX;

            e.data.setTranslate( e.data.currentX,  e.data.track);
        }
    }

    /**
     * Updates new position of draggable item
     * @param {number} xPos
     * @param {object} el
     */
    setTranslate(xPos, el) {
        el[0].style.transform = "translate3d(" + xPos + "px, " + 0 + "px, 0)";
    }

    // End DRAG UX
}

const slides = [
    {
        name: 'slide1',
        content: `<svg height="300" width="300">
                            <defs>
                                <radialGradient id="grad1" cx="50%" cy="50%" r="50%" fx="50%" fy="50%">
                                    <stop offset="0%" style="stop-color:rgb(255,255,255);stop-opacity:0" />
                                    <stop offset="100%" style="stop-color:rgb(0,0,255);stop-opacity:1" />
                                </radialGradient>
                            </defs>
                            <ellipse cx="150" cy="150" rx="100" ry="100" fill="url(#grad1)" />
                            Sorry, your browser does not support inline SVG.
                        </svg>`
    },
    {
        name: 'slide2',
        url: 'img/gif1.gif'
    },
    {
        name: 'slide3',
        url: 'img/gif2.gif'
    },
    {
        name: 'slide4',
        url: 'img/jpg2.jpg'
    },
    {
        name: 'slide5',
        url: 'img/png1.png'
    },
    {
        name: 'slide6',
        url: 'img/jpg3.jpg'
    },
    {
        name: 'slide7',
        url: 'img/jpg4.jpg'
    },
    {
        name: 'slide8',
        url: 'img/jpg5.jpg'
    },
    {
        name: 'slide9',
        url: 'img/jpg6.jpg'
    },
    {
        name: 'slide10',
        url: 'img/jpg7.jpg'
    },
    {
        name: 'slide11',
        url: 'img/jpg8.jpg'
    },
    {
        name: 'slide12',
        content: `<iframe width="560" height="315" src="https://www.youtube.com/embed/a67W7gRzK1w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
    }
];

const customSlider = new CustomSlider(slides);

customSlider.build();