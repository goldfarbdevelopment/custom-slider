<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Greg's Custom Slider</title>
    <link rel="stylesheet" href="css/style.css"/>
</head>
<body>
    <div class="container-fluid">
        <div class="custom-slider">
            <div class="row">
                <div class="col-2 custom-slider-button custom-slider-button-left">
                    <i class="icon-left-open h1"></i>
                </div>
                <div class="col-8 custom-slider-slides text-center">
                    <div class="custom-slider-track">

                    </div>
                </div>
                <div class="col-2 custom-slider-button custom-slider-button-right">
                    <i class="icon-right-open h1"></i>
                </div>
            </div>
            <div class="custom-slider-navigation"></div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <script type="module" src="js/custom-slider.js"></script>
</body>
</html>