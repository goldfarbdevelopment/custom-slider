# custom-slider

Custom Slider is a generic example of a web-based slider that is draggable and has a paginated navigation

#Design decisions
Simplified column design with Bootstrap 4

Made left/right nav buttons columns and full screen height for clarity of button locations

Avoided using icons or fancy css for bottom pagination by using simple radio buttons.

#Future improvement
With more time I would make the functionality much more dynamic.

Create a drag and drop feature to drop in a JSON object to build slider.

Separate drag functions into a separate class. 

Make responsive screensize change after loading.  

Make drag UX more flawless including making the video embed and svg draggable. 


